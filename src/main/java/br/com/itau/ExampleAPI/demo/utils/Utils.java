package br.com.itau.ExampleAPI.demo.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	public static String formartarData() {
		DateFormat formatarData = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return formatarData.format(date);
	}
}
