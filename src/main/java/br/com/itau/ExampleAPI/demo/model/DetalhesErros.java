package br.com.itau.ExampleAPI.demo.model;

public class DetalhesErros {
	
	private String titulo;
	private int status;
	private String detalhe;
	private String timestamp;
	private String mensagem;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDetalhe() {
		return detalhe;
	}
	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public static final class Builder {
		
		private String titulo;
		private int status;
		private String detalhe;
		private String timestamp;
		private String mensagem;
		
		private Builder() {
		}
		public static Builder newBuilder() {
			return new Builder();
		}
		public Builder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}
		public Builder status(int status) {
			this.status = status;
			return this;
		}
		public Builder detalhe(String detalhe) {
			this.detalhe = detalhe;
			return this;
		}
		public Builder timestamp(String timestamp) {
			this.timestamp = timestamp;
			return this;
		}
		public Builder mensagem(String mensagem) {
			this.mensagem = mensagem;
			return this;
		}
		public DetalhesErros build() {
			DetalhesErros erros = new DetalhesErros();
			erros.setTitulo(titulo);
			erros.setStatus(status);
			erros.setDetalhe(detalhe);
			erros.setTimestamp(timestamp);
			erros.setMensagem(mensagem);
			return erros;
		}
	}
}
