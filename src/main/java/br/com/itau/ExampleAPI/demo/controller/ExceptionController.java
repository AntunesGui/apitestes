package br.com.itau.ExampleAPI.demo.controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.itau.ExampleAPI.demo.model.DetalhesErros;
import br.com.itau.ExampleAPI.demo.utils.Utils;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler{

	@Override
	public ResponseEntity<Object> handleNoHandlerFoundException(
			NoHandlerFoundException ex, 
			HttpHeaders headers, 
			HttpStatus status, 
			WebRequest request) {
		DetalhesErros erros = DetalhesErros.Builder
				.newBuilder()
				.timestamp(Utils.formartarData())
				.status(HttpStatus.NOT_FOUND.value())
				.titulo("Erro na chamada do banco de dados")
				.detalhe("Erro na chamada do banco de dados")
				.mensagem(ex.getClass().getName())
				.build();
		return new ResponseEntity<>(erros, HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
		DetalhesErros erros = DetalhesErros.Builder
				.newBuilder()
				.timestamp(Utils.formartarData())
				.status(HttpStatus.BAD_REQUEST.value())
				.titulo("Erro na chamada do banco de ")
				.detalhe("Erro na chamada do banco de ")
				.mensagem(ex.getClass().getName())
				.build();
		return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> handleExceptionInternal(Exception ex, 
			HttpHeaders headers, 
			HttpStatus status) {
		DetalhesErros erros = DetalhesErros.Builder
				.newBuilder()
				.timestamp(Utils.formartarData())
				.status(HttpStatus.BAD_REQUEST.value())
				.titulo("Validacao")
				.detalhe("campo id com valor de String")
				.mensagem(ex.getMessage())
				.build();
		return new ResponseEntity<>(erros, headers, status);
	}
	
	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ResponseEntity<Object> selectVazio(Exception ex,
			HttpHeaders headers, 
			HttpStatus status){
		DetalhesErros erros = DetalhesErros.Builder
				.newBuilder()
				.timestamp(Utils.formartarData())
				.status(HttpStatus.BAD_REQUEST.value())
				.titulo("Validacao")
				.detalhe("campo id com valor de String")
				.mensagem(ex.getMessage())
				.build();
		return new ResponseEntity<>(erros, headers, status);
		
	}
}
