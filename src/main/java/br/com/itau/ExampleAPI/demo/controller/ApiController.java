package br.com.itau.ExampleAPI.demo.controller;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.ExampleAPI.demo.model.DetalhesErros;
import br.com.itau.ExampleAPI.demo.model.OfertaDaoModel;
import br.com.itau.ExampleAPI.demo.utils.Utils;

@RestController
public class ApiController {

	private String retornoChamadaSelect;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger logger = Logger.getLogger(ApiController.class.getName());
	
	@GetMapping(path = "procurar/id/{id}")
	public ResponseEntity<?> procurarPorID(@PathVariable("id") Integer id) {

		try {
			logger.log(Level.SEVERE,"ID recebido: ",id);
			retornoChamadaSelect = jdbcTemplate.queryForObject("select json from ofertas WHERE id="+id, String.class);
		} catch(EmptyResultDataAccessException ex) {
			logger.log(Level.SEVERE,"Query executada: ", retornoChamadaSelect);
			DetalhesErros erros =  DetalhesErros.Builder
					.newBuilder()
					.timestamp(Utils.formartarData())
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.titulo("Erro na chamada do banco de dados")
					.detalhe("Retorno do select vazio")
					.mensagem(ex.getClass().getName())
					.build();
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(erros);
		}
		return ResponseEntity.ok(retornoChamadaSelect);
	}
	
	@PostMapping(path = "salvar/id")
	public ResponseEntity<?> salvarOfertaNoBanco(@RequestBody OfertaDaoModel oferta) {
		
		String sql = "INSERT INTO ofertas " +
				"(id, json) VALUES (?, ?)";
		
		jdbcTemplate.update(sql, new Object[] { 
				oferta.getId(),
				oferta.getJson()  
			});
		
		return ResponseEntity.ok("salvo");
	}

	@GetMapping(path= "procurar/ids/{id}")
	public ResponseEntity<?> procurarVariosIDs(@PathVariable("id") ArrayList<Long> id) {

		return ResponseEntity.ok("Requisicao ok");
	}
}
