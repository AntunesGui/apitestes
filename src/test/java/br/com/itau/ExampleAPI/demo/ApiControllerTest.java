package br.com.itau.ExampleAPI.demo;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.itau.ExampleAPI.demo.controller.ApiController;

public class ApiControllerTest {

	@Autowired
	private TestRestTemplate restTamplate;

	@MockBean
    ApiController apiController;

	@Autowired
	private MockMvc mockMvc;
	
	// Maneira usando BDD
	@Test
	public void procurarPorId() throws Exception {
//		ResponseEntity retorno = new ResponseEntity(HttpStatus.OK);
//		BDDMockito.when(apiController.procurarPorID(1)).thenReturn(retorno);
		mockMvc.perform(MockMvcRequestBuilders
				.get("/procurar/id/{id}", 1))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	// Maneira usando restTamplate
	@Test
	public void erroProcurarPorId() {
		ResponseEntity<?> response = restTamplate.getForEntity("/procurar/id/{id}", String.class, "g");
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
	}
}
