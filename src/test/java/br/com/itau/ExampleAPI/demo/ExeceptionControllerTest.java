package br.com.itau.ExampleAPI.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.MethodArgumentNotValidException;

import br.com.itau.ExampleAPI.demo.controller.ApiController;
import br.com.itau.ExampleAPI.demo.controller.ExceptionController;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@OverrideAutoConfiguration(enabled=true)
public class ExeceptionControllerTest {

	@Autowired
	ExceptionController exception;
	
	@MockBean
	ApiController apiController;
	
	@Test
	public void handleExceptionInternal() {
		exception.handleExceptionInternal(new Exception(), new HttpHeaders(), HttpStatus.ACCEPTED);
	}
	
	@Test
	public void handleMethodArgumentNotValid() {
		exception.handleMethodArgumentNotValid(new MethodArgumentNotValidException(null, null));
	}
	
	@Test
	public void selectVazio() {
//		JdbcTemplate template = Mockito.mock(JdbcTemplate.class);
//		Mockito.when(template.queryForObject("teste", String.class)).thenThrow(EmptyResultDataAccessException.class);
//		Mockito.when(exception.selectVazio(new Exception(), new HttpHeaders(), HttpStatus.ACCEPTED)).thenThrow(EmptyResultDataAccessException.class);
		
	}
}
